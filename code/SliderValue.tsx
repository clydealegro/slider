import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"

export function SliderValue(props) {
    return (
        <Frame
            background={"none"}
            size={80}
            style={{ fontSize: "32px", color: "#fff", fontWeight: "bold" }}
        >
            {props.value}%
        </Frame>
    )
}

SliderValue.defaultProps = {
    value: 0,
}
