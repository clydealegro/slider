import * as React from "react"
import { Frame, addPropertyControls, ControlType, transform } from "framer"

// Open Preview: Command + P
// Learn more: https://framer.com/api

export function Slider(props) {
    const { progress, dragSlider } = props

    function handleDrag(event, info) {
        dragSlider(event, info)
    }

    return (
        <Frame width={300} height={10} background={"#f2f2f2"} borderRadius={30}>
            <Frame
                background={"#49A8E1"}
                width={progress + "%"}
                height={10}
                borderRadius={30}
            />
            <Frame
                size={30}
                borderRadius={50}
                background={"#fff"}
                drag="x"
                dragConstraints={{ left: -15, right: 285 }}
                dragElastic={0}
                x={-15}
                y={-10}
                onDrag={handleDrag}
            />
        </Frame>
    )
}

Slider.defaultProps = {
    progress: 0,
    dragSlider: (event, info) => null,
}
