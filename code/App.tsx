import * as React from "react"
import { Override, Data, transform } from "framer"

const appState = Data({
    progress: 0,
})

export function Slider(): Override {
    return {
        progress: appState.progress,
        dragSlider: (event, info) => {
            const inputRange = [-15, 285]
            const outputRange = [0, 100]
            const output = transform(info.point.x, inputRange, outputRange)
            appState.progress = Math.round(output)
        },
    }
}

export function SliderValue(): Override {
    return {
        value: appState.progress,
    }
}
